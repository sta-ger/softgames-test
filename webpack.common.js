const path = require("path");

// noinspection WebpackConfigHighlighting,DuplicatedCode
module.exports = {
    entry: "./src/index.ts",
    output: {
        library: "game",
        filename: "game.js",
        path: path.resolve(__dirname, "dist"),
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    module: {
        rules: [
            {
                use: "ts-loader",
                include: [path.resolve("src")],
            },
        ],
    },
};
