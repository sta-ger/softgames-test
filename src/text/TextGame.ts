import {Application, Loader} from "pixi.js";
import {TextElementsParser} from "./TextElementsParser";
import {ElementTypeDetector} from "./ElementTypeDetector";
import {TextAssetsFactory} from "./TextAssetsFactory";
import {TextAssetsAligner} from "./TextAssetsAligner";
import {SmartText} from "./SmartText";
import {gsap} from "gsap";

export class TextGame {
    private readonly application = new Application();
    private readonly onResizeDelegate = () => this.onResize(window.innerWidth, window.innerHeight);
    private readonly gameContainer = document.getElementById("game") as HTMLDivElement;
    private readonly availableWords = "lorem ipsum dolor sit amet consectetur adipiscing elit".split(" ");
    private nextStepTween?: gsap.core.Tween;

    private text = new SmartText(
        new TextElementsParser(new ElementTypeDetector()),
        new TextAssetsFactory(),
        new TextAssetsAligner(),
    );

    public start() {
        const loader = new Loader();
        loader.add("assets/smiley.png");
        loader.load(() => {
            this.application.view.style.width = "100%";
            this.application.view.style.height = "100%";
            this.gameContainer.appendChild(this.application.view);

            this.application.stage.addChild(this.text.getDisplay());

            const fn = () => {
                this.text.setText(this.createRandomText());
                this.alignText();
                this.nextStepTween = gsap.delayedCall(1, () => fn());
            };
            fn();

            this.onResize(window.innerWidth, window.innerHeight);
            window.addEventListener("resize", this.onResizeDelegate);
        });
    }

    public destroy(): void {
        window.removeEventListener("resize", this.onResizeDelegate);
        this.nextStepTween?.kill();
        this.gameContainer.removeChild(this.application.view);
    }

    private onResize(w: number, h: number): void {
        this.application.renderer.resize(w, h);
        this.alignText();
    }

    private alignText(): void {
        const w = this.application.renderer.width;
        const h = this.application.renderer.height;
        this.text.getDisplay().x = w / 2 - this.text.getDisplay().getBounds().width / 2;
        this.text.getDisplay().y = h / 2 - this.text.getDisplay().getBounds().height / 2;
    }

    private createRandomText(): string {
        const words = new Array(TextGame.random(5, 0))
            .fill(0)
            .map(() => this.availableWords[TextGame.random(this.availableWords.length)]);
        const smileys = new Array(TextGame.random(3, 0)).fill(0).map(() => "<img>");
        let text = [...words, ...smileys].sort(() => 0.5 - Math.random()).join(" ");
        while (text === "") {
            text = this.createRandomText();
        }
        return text[0].toUpperCase() + text.substring(1, text.length);
    }

    private static random(to: number, from = 0): number {
        return Math.floor(Math.random() * (from - to)) + to;
    }
}
