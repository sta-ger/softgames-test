import {DisplayObject} from "pixi.js";

export class TextAssetsAligner {
    private readonly padding: number;

    constructor(padding = 7) {
        this.padding = padding;
    }

    public alignAssets(assets: DisplayObject[]): void {
        assets.forEach((asset, i) => {
            let prevAsset: DisplayObject;
            if (i > 0) {
                prevAsset = assets[i - 1];
                asset.x = prevAsset.x + prevAsset.getBounds().width + this.padding;
                asset.y = prevAsset.y + prevAsset.getBounds().height / 2 - asset.getBounds().height / 2;
            } else {
                asset.x = 0;
                asset.y = 0;
            }
        });
    }
}
