import {TextElementType} from "./TextElementType";

export class TextElement {
    private readonly value: string;
    private readonly type: TextElementType;

    constructor(value: string, type: TextElementType) {
        this.value = value;
        this.type = type;
    }

    public getValue(): string {
        return this.value;
    }

    public getType(): TextElementType {
        return this.type;
    }
}
