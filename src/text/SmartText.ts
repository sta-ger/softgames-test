import {TextElementsParser} from "./TextElementsParser";
import {TextAssetsFactory} from "./TextAssetsFactory";
import {Container, DisplayObject} from "pixi.js";
import {TextAssetsAligner} from "./TextAssetsAligner";

export class SmartText {
    private readonly container = new Container();
    private readonly elementsParser: TextElementsParser;
    private readonly assetsProvider: TextAssetsFactory;
    private readonly assetsAligner: TextAssetsAligner;

    constructor(
        elementsParser: TextElementsParser,
        assetsProvider: TextAssetsFactory,
        assetsAligner: TextAssetsAligner,
    ) {
        this.elementsParser = elementsParser;
        this.assetsProvider = assetsProvider;
        this.assetsAligner = assetsAligner;
    }

    public setText(value: string): void {
        while (this.container.children.length > 0) {
            this.container.removeChildAt(0);
        }
        const elements = this.elementsParser.parseText(value);
        const assets = elements.map((element) => this.assetsProvider.createAsset(element));
        this.assetsAligner.alignAssets(assets);
        assets.forEach((asset) => this.container.addChild(asset));
    }

    public getDisplay(): DisplayObject {
        return this.container;
    }
}
