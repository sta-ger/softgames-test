import {TextElementType} from "./TextElementType";

export class ElementTypeDetector {
    getElementType(word: string): TextElementType {
        if (word === "<img>") {
            return TextElementType.Image;
        } else {
            return TextElementType.Word;
        }
    }
}
