import {DisplayObject, Sprite, Text} from "pixi.js";
import {TextElement} from "./TextElement";
import {TextElementType} from "./TextElementType";

export class TextAssetsFactory {
    public createAsset(element: TextElement): DisplayObject {
        if (element.getType() === TextElementType.Word) {
            return new Text(element.getValue(), {fill: 0xffffff});
        } else {
            return Sprite.from("assets/smiley.png");
        }
    }
}
