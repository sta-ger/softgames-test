import {TextElement} from "./TextElement";
import {ElementTypeDetector} from "./ElementTypeDetector";

export class TextElementsParser {
    private readonly elementTypeDetector: ElementTypeDetector;

    constructor(elementTypeDetector: ElementTypeDetector) {
        this.elementTypeDetector = elementTypeDetector;
    }

    public parseText(text: string): TextElement[] {
        return text.split(" ").map((word) => new TextElement(word, this.elementTypeDetector.getElementType(word)));
    }
}
