import {Application, Container, Loader, Text, Ticker} from "pixi.js";
import {Card} from "./Card";
import {Deck} from "./Deck";
import {CardsAligner} from "./CardsAligner";
import {CardMover} from "./CardMover";
import {gsap} from "gsap";

export class DeckGame {
    private readonly application = new Application();
    private readonly leftDeck = new Deck([]);
    private readonly rightDeck = new Deck([]);
    private readonly cardsAligner = new CardsAligner();
    private readonly cardMover = new CardMover(this.cardsAligner);
    private readonly onResizeDelegate = () => this.onResize(window.innerWidth, window.innerHeight);
    private readonly gameContainer = document.getElementById("game") as HTMLDivElement;
    private nextStepTween?: gsap.core.Tween;

    public start() {
        const loader = new Loader();
        loader.add("card", "assets/card.png");
        loader.load(() => {
            for (let i = 0; i < 144; i++) {
                this.leftDeck.addCard(new Card());
            }
            this.application.view.style.width = "100%";
            this.application.view.style.height = "100%";
            this.gameContainer.appendChild(this.application.view);

            const cardsContainer = new Container();

            this.leftDeck.getCards().forEach((card) => cardsContainer.addChild(card.getDisplay()));
            this.rightDeck.getCards().forEach((card) => cardsContainer.addChild(card.getDisplay()));

            const fps = new Text("", {fill: 0xffffff});
            let lastTime = performance.now();
            Ticker.shared.add(() => {
                const time = performance.now();
                if (time - lastTime >= 1000) {
                    fps.text = Math.round(Ticker.shared.FPS) + " FPS";
                    lastTime = time;
                }
            });

            this.application.stage.addChild(cardsContainer);
            this.application.stage.addChild(fps);

            this.onResize(window.innerWidth, window.innerHeight);
            window.addEventListener("resize", this.onResizeDelegate);

            const fn = () => {
                if (this.leftDeck.getCards().length > 0) {
                    this.cardMover.moveCard(this.leftDeck, this.rightDeck);
                    this.nextStepTween = gsap.delayedCall(1, () => fn());
                }
            };
            fn();
        });
    }

    public destroy(): void {
        window.removeEventListener("resize", this.onResizeDelegate);
        this.cardMover.reset();
        this.nextStepTween?.kill();
        this.gameContainer.removeChild(this.application.view);
    }

    private onResize(w: number, h: number): void {
        this.application.renderer.resize(w, h);

        this.cardMover.reset();

        const halfWidth = w / 2;

        this.leftDeck.x = halfWidth / 2;
        this.leftDeck.y = window.innerHeight;
        this.cardsAligner.alignCards(this.leftDeck);

        this.rightDeck.x = halfWidth + halfWidth / 2;
        this.rightDeck.y = window.innerHeight;
        this.cardsAligner.alignCards(this.rightDeck);
    }
}
