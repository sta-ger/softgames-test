import {Deck} from "./Deck";
import {Card} from "./Card";

export class CardsAligner {
    private readonly cardsPadding: number;

    constructor(cardsPadding = 1) {
        this.cardsPadding = cardsPadding;
    }

    public getCardPosition(card: Card, cardId: number, deck: Deck): {x: number; y: number} {
        const y = deck.y - card.getDisplay().getBounds().height - this.cardsPadding * cardId;
        const x = deck.x - card.getDisplay().getBounds().width / 2;
        return {x, y};
    }

    public alignCards(deck: Deck): void {
        deck.getCards().forEach((card, i) => {
            const pos = this.getCardPosition(card, i, deck);
            card.getDisplay().x = pos.x;
            card.getDisplay().y = pos.y;
        });
    }
}
