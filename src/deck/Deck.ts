import {Card} from "./Card";

export class Deck {
    public x = 0;
    public y = 0;

    private readonly cards: Card[];

    constructor(cards: Card[]) {
        this.cards = cards;
    }

    public getCards(): Card[] {
        return this.cards;
    }

    public addCard(card: Card): void {
        this.cards.push(card);
    }

    public getCardId(card: Card): number {
        return this.cards.indexOf(card);
    }

    public getLastCard(): Card | undefined {
        return this.cards.pop();
    }
}
