import {Deck} from "./Deck";
import {gsap} from "gsap";
import {CardsAligner} from "./CardsAligner";
import {Card} from "./Card";

export class CardMover {
    private readonly cardsAligner: CardsAligner;
    private readonly movingCards = new Map<Card, {tween: gsap.core.Tween; sourceDeck: Deck}>();

    constructor(cardsAligner: CardsAligner) {
        this.cardsAligner = cardsAligner;
    }

    public moveCard(sourceDeck: Deck, targetDeck: Deck): void {
        const sourceCardId = sourceDeck.getCards().length - 1;
        if (sourceCardId >= 0) {
            const card = sourceDeck.getLastCard() as Card;
            card.getDisplay().parent.addChild(card.getDisplay());
            const targetCardId = targetDeck.getCards().length - 1 + this.movingCards.size;
            const targetPosition = this.cardsAligner.getCardPosition(card, targetCardId, targetDeck);
            const tween = gsap.to(card.getDisplay(), {
                ...targetPosition,
                duration: 2,
                onComplete: () => this.onMovementFinished(card, targetDeck),
            });
            this.movingCards.set(card, {tween, sourceDeck});
        } else {
            throw "Can't get card from deck";
        }
    }

    public reset(): void {
        this.movingCards.forEach(({tween, sourceDeck}, card) => {
            tween.kill();
            sourceDeck.addCard(card);
        });
        this.movingCards.clear();
    }

    private onMovementFinished(card: Card, targetDeck: Deck): void {
        targetDeck.addCard(card);
        this.movingCards.delete(card);
    }
}
