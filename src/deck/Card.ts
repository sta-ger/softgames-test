import {DisplayObject, Sprite} from "pixi.js";

export class Card {
    private sprite = Sprite.from("card");

    public getDisplay(): DisplayObject {
        return this.sprite;
    }
}
