import {DeckGame} from "./deck/DeckGame";
import {TextGame} from "./text/TextGame";
import {FireGame} from "./fire/FireGame";

export {DeckGame, TextGame, FireGame};
