import {Application, Loader, Text} from "pixi.js";
import {Flame} from "./Flame";
import {Explosion} from "./Explosion";

export class FireGame {
    private readonly application = new Application();
    private readonly onResizeDelegate = () => this.onResize(window.innerWidth, window.innerHeight);
    private readonly gameContainer = document.getElementById("game") as HTMLDivElement;
    private clickText = new Text("Press anywhere", {fill: 0xffffff});
    private flame!: Flame;

    constructor() {
        this.clickText.anchor.x = 0.5;
        this.clickText.anchor.y = 0.5;
    }

    public start() {
        const loader = new Loader();
        loader.add("assets/particle.png");
        loader.add("assets/Fire.png");
        loader.load(() => {
            this.flame = new Flame();
            this.application.view.style.width = "100%";
            this.application.view.style.height = "100%";
            this.gameContainer.appendChild(this.application.view);
            this.application.stage.addChild(this.flame.getDisplay());

            this.application.stage.addChild(this.clickText);

            window.onpointerdown = (e) => {
                this.application.stage.removeChild(this.clickText);
                if (!this.flame.isStarted()) {
                    this.flame.start();
                }
                this.flame.setPosition(e.clientX, e.clientY);
                new Explosion(e.clientX, e.clientY, this.application.stage);
            };

            this.onResize(window.innerWidth, window.innerHeight);
            window.addEventListener("resize", this.onResizeDelegate);
        });
    }

    public destroy(): void {
        window.removeEventListener("resize", this.onResizeDelegate);
        this.gameContainer.removeChild(this.application.view);
        this.flame.destroy();
    }

    private onResize(w: number, h: number): void {
        this.application.renderer.resize(w, h);
        this.clickText.x = w / 2;
        this.clickText.y = h / 2;
    }
}
