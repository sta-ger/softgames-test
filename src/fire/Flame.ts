import {Emitter} from "@pixi/particle-emitter";
import {Container, DisplayObject} from "pixi.js";
import {Glow} from "./Glow";

export class Flame {
    private container = new Container();
    private emitter: Emitter;
    private animationFrameRequestId?: number;
    private glow: Glow;

    constructor() {
        const config = {
            lifetime: {
                min: 0,
                max: 0.75,
            },
            frequency: 0.0001,
            emitterLifetime: 0,
            maxParticles: 10000,
            pos: {
                x: 0,
                y: 0,
            },
            addAtBack: false,
            behaviors: [
                {
                    type: "alpha",
                    config: {
                        alpha: {
                            list: [
                                {
                                    time: 0,
                                    value: 0.62,
                                },
                                {
                                    time: 1,
                                    value: 0,
                                },
                            ],
                        },
                    },
                },
                {
                    type: "scale",
                    config: {
                        scale: {
                            list: [
                                {
                                    time: 0,
                                    value: 0.25,
                                },
                                {
                                    time: 1,
                                    value: 0.75,
                                },
                            ],
                        },
                        minMult: 1,
                    },
                },
                {
                    type: "color",
                    config: {
                        color: {
                            list: [
                                {
                                    time: 0,
                                    value: "fff191",
                                },
                                {
                                    time: 1,
                                    value: "ff622c",
                                },
                            ],
                        },
                    },
                },
                {
                    type: "moveSpeedStatic",
                    config: {
                        min: 0,
                        max: 300,
                    },
                },
                {
                    type: "rotation",
                    config: {
                        accel: 0,
                        minSpeed: 0,
                        maxSpeed: 50,
                        minStart: 265,
                        maxStart: 275,
                    },
                },
                {
                    type: "textureRandom",
                    config: {
                        textures: ["assets/particle.png", "assets/Fire.png"],
                    },
                },
                {
                    type: "spawnShape",
                    config: {
                        type: "torus",
                        data: {
                            x: 0,
                            y: 0,
                            radius: 50,
                            innerRadius: 0,
                            affectRotation: false,
                        },
                    },
                },
            ],
        };

        this.emitter = new Emitter(this.container, config);
        this.emitter.emit = false;

        this.glow = new Glow();
        this.container.addChild(this.glow.getDisplay());

        let elapsed = Date.now();
        const update = () => {
            this.animationFrameRequestId = requestAnimationFrame(update);
            const now = Date.now();
            this.emitter.update((now - elapsed) * 0.001);
            elapsed = now;
        };
        update();
    }

    public start(): void {
        this.emitter.emit = true;
        this.glow.start();
    }

    public isStarted(): boolean {
        return this.emitter.emit;
    }

    public setPosition(x: number, y: number): void {
        this.emitter.updateSpawnPos(x, y);
        this.glow.getDisplay().x = x;
        this.glow.getDisplay().y = y;
    }

    public destroy(): void {
        this.glow.destroy();
        this.emitter.emit = false;
        if (this.animationFrameRequestId) {
            cancelAnimationFrame(this.animationFrameRequestId);
        }
    }

    public getDisplay(): DisplayObject {
        return this.container;
    }
}
