import {Container, DisplayObject, Sprite} from "pixi.js";
import {gsap, Linear} from "gsap";

export class Glow {
    private readonly container: Container;
    private nextFlickTween?: gsap.core.Tween;
    private tween?: gsap.core.Tween;

    constructor(diameter = 500) {
        this.container = new Container();
        const canvas = document.createElement("canvas");
        canvas.width = diameter;
        canvas.height = diameter;
        const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
        const gradient = ctx.createRadialGradient(
            diameter / 2,
            diameter / 2,
            0,
            diameter / 2,
            diameter / 2,
            diameter / 2,
        );
        gradient.addColorStop(0, "rgb(255, 98, 44)");
        gradient.addColorStop(1, "rgb(255, 98, 44, 0)");
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, diameter, diameter);
        const s = Sprite.from(canvas);
        s.anchor.x = 0.5;
        s.anchor.y = 0.5;
        this.container.addChild(s);
        this.container.alpha = 0;
    }

    public start(): void {
        this.doFlick();
    }

    public destroy(): void {
        this.nextFlickTween?.kill();
        this.tween?.kill();
    }

    public getDisplay(): DisplayObject {
        return this.container;
    }

    private getFlickTweenStartValue(): number {
        return 0.25 + Math.random() * 0.25;
    }

    private getFlickTweenEndValue(): number {
        return 0.1 + Math.random() * 0.15;
    }

    private getFlickTweenDuration(): number {
        return Math.random() / 2;
    }

    private getFlickDelay(): number {
        return Math.random() * 0.15;
    }

    private doFlick(): void {
        this.tween?.kill();
        this.container.alpha = this.getFlickTweenStartValue();
        this.tween = gsap.to(this.container, {
            alpha: this.getFlickTweenEndValue(),
            ease: Linear.easeNone,
            duration: this.getFlickTweenDuration(),
            onComplete: () => {
                this.nextFlickTween = gsap.delayedCall(this.getFlickDelay(), () => this.doFlick());
            },
        });
    }
}
