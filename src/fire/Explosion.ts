import {Container, Graphics} from "pixi.js";
import {gsap, Linear, Quint} from "gsap";

export class Explosion {
    constructor(x: number, y: number, parentContainer: Container) {
        const radius = 100;

        const container = new Container();
        parentContainer.addChild(container);

        const filledCirc = new Graphics().beginFill(0xfff191).drawCircle(0, 0, radius).endFill();
        const circ = new Graphics().lineStyle({color: 0xff622c, width: 1}).drawCircle(0, 0, radius).endFill();
        container.addChild(filledCirc);
        container.addChild(circ);

        container.position = {x, y};

        filledCirc.scale.x = 0.9;
        filledCirc.scale.y = 0.9;
        filledCirc.alpha = 0;

        circ.alpha = 0.5;

        const maxScale = 1;
        const minScale = 0.0;
        const appearTime = 0.1;
        const fadeTime = 1;

        let tl = gsap.timeline();
        tl.to(filledCirc.scale, {
            x: maxScale,
            y: maxScale,
            duration: appearTime,
            ease: Linear.easeNone,
            onComplete: () => {
                gsap.to(circ.scale, {x: 2, y: 2, duration: 3, ease: Quint.easeOut});
                gsap.to(circ, {
                    alpha: 0,
                    duration: 3,
                    ease: Quint.easeOut,
                    onComplete: () => {
                        parentContainer.removeChild(container);
                    },
                });
            },
        });
        tl.to(filledCirc.scale, {x: minScale, y: minScale, duration: fadeTime, ease: Quint.easeOut});

        tl = gsap.timeline();
        tl.to(filledCirc, {alpha: 1, duration: appearTime, ease: Linear.easeNone});
        tl.to(filledCirc, {alpha: 0, duration: fadeTime, ease: Quint.easeOut});
    }
}
