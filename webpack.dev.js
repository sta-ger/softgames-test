// noinspection JSUnresolvedFunction

const webpack = require("webpack");
const {merge} = require("webpack-merge");
const common = require("./webpack.common.js");
const path = require("path");

// noinspection JSCheckFunctionSignatures
module.exports = merge(common, {
    mode: "development",
    devtool: false,
    plugins: [new webpack.SourceMapDevToolPlugin()],
    devServer: {
        static: {
            directory: path.join(__dirname, "."),
            watch: false,
        },
        watchFiles: ["src/**/*.ts", "index.html"],
        compress: true,
        port: process.env.PORT || 8000,
        host: "0.0.0.0",
    },
});
